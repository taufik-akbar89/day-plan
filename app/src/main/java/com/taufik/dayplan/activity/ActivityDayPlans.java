package com.taufik.dayplan.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.taufik.dayplan.DayPlan;
import com.taufik.dayplan.R;
import com.taufik.dayplan.Tools;
import com.taufik.dayplan.adapter.DayPlanAdapter;
import com.taufik.dayplan.adapter.TripAdapter;
import com.taufik.dayplan.data.DataGenerator;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ActivityDayPlans extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        initToolbar();
        initComponent();
    }
    
    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle("Day Plans");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        //change menu button color
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.colorWhite));
        //change overflow menu button color
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
    
    private void initComponent() {
        // define recycler view
        RecyclerView recycler_view_day = findViewById(R.id.recycler_view_day);
        RecyclerView recycler_view_main = findViewById(R.id.recycler_view_main);
        RecyclerView recycler_view_city = findViewById(R.id.recycler_view_city);
        final RecyclerView recycler_view_trip = findViewById(R.id.recycler_view_trip);
        Tools.configRecycler(this, recycler_view_day, LinearLayoutManager.HORIZONTAL);
        Tools.configRecycler(this, recycler_view_main, LinearLayoutManager.HORIZONTAL);
        Tools.configRecycler(this, recycler_view_city, LinearLayoutManager.HORIZONTAL);
        Tools.configRecycler(this, recycler_view_trip, LinearLayoutManager.VERTICAL);
        
        List<DayPlan> items = DataGenerator.getAdayInJakarta(this);
        List<DayPlan> itemsMain = DataGenerator.getMainSight(this);
    
        //set data and list adapter
        DayPlanAdapter adapter = new DayPlanAdapter(this, items);
        DayPlanAdapter adapterMain = new DayPlanAdapter(this, itemsMain);
        TripAdapter adapterTrip = new TripAdapter(this, itemsMain);
        recycler_view_day.setAdapter(adapter);
        recycler_view_city.setAdapter(adapter);
        recycler_view_main.setAdapter(adapterMain);
        recycler_view_trip.setAdapter(adapterTrip);
    
        final ImageView imgDropMain = findViewById(R.id.image_drop);
        MaterialRippleLayout rippleMain = findViewById(R.id.ripple_main);
        rippleMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
    
    
                if (recycler_view_trip.getVisibility() == View.VISIBLE) {
                    imgDropMain.animate().rotation(0f).start();
                    Tools.slideUp(recycler_view_trip);
                    recycler_view_trip.setVisibility(View.GONE);
                } else {
                    imgDropMain.animate().rotation(180f).start();
                    Tools.slideDown(recycler_view_trip);
                    recycler_view_trip.setVisibility(View.VISIBLE);
                }
            }
        });
        
        
    }
    
   
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dayplan_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.colorWhite));
        return true;
    }
    
}
