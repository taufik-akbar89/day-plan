package com.taufik.dayplan;

/**
 * ============================
 * Created by Taufik Akbar
 * taufik.akbar89@gmail.com
 * 2020-02-11
 * ============================
 */

public class DayPlan {
    
    public String name;
    public int image;
    public String type;
    public String timeInfo;
    public String waitingHour;
    public String timeDistance;
    
}
