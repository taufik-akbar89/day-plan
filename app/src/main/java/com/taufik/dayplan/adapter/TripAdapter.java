package com.taufik.dayplan.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taufik.dayplan.DayPlan;
import com.taufik.dayplan.R;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * ============================
 * Created by Taufik Akbar
 * taufik.akbar89@gmail.com
 * 2020-02-11
 * ============================
 */

public class TripAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    
    private List<DayPlan> items;
    
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    
    public interface OnItemClickListener {
        void onItemClick(View view, DayPlan obj, int position);
    }
    
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }
    
    public TripAdapter(Context context, List<DayPlan> items) {
        this.items = items;
        ctx = context;
    }
    
    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView timeDistance;
        public TextView waitingHours;
        public TextView openingTime;
        public TextView number;
        
        public ItemViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
            timeDistance = v.findViewById(R.id.distance);
            waitingHours = v.findViewById(R.id.waiting_time);
            openingTime = v.findViewById(R.id.opening_time);
            number = v.findViewById(R.id.number);
        }
    }
    
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip, parent, false);
        vh = new ItemViewHolder(v);
        return vh;
    }
    
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            ItemViewHolder view = (ItemViewHolder) holder;
            
            DayPlan p = items.get(position);
            view.name.setText(p.name);
            view.waitingHours.setText(p.waitingHour);
            view.number.setText(String.valueOf(position+1));
            
            String timeDistance = p.timeDistance + " by " + p.type;
            view.timeDistance.setText(timeDistance);
    
            if(position %2 == 1) {
                holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                holder.itemView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
                //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
            }
        }
    }
    
    @Override
    public int getItemCount() {
        return items.size();
    }
}
