package com.taufik.dayplan.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.taufik.dayplan.DayPlan;
import com.taufik.dayplan.R;
import com.taufik.dayplan.Tools;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * ============================
 * Created by Taufik Akbar
 * taufik.akbar89@gmail.com
 * 2020-02-11
 * ============================
 */

public class DayPlanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    
    private List<DayPlan> items;
    
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    
    public interface OnItemClickListener {
        void onItemClick(View view, DayPlan obj, int position);
    }
    
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }
    
    public DayPlanAdapter(Context context, List<DayPlan> items) {
        this.items = items;
        ctx = context;
    }
    
    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        
        public ItemViewHolder(View v) {
            super(v);
            image = v.findViewById(R.id.image);
        }
    }
    
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_dayplan, parent, false);
        vh = new ItemViewHolder(v);
        return vh;
    }
    
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            ItemViewHolder view = (ItemViewHolder) holder;
            
            DayPlan p = items.get(position);
            Tools.displayImageOriginal(ctx, view.image, p.image);
        }
    }
    
    @Override
    public int getItemCount() {
        return items.size();
    }
}
