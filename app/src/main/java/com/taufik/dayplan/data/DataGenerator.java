package com.taufik.dayplan.data;

import android.content.Context;
import android.content.res.TypedArray;

import com.taufik.dayplan.DayPlan;
import com.taufik.dayplan.R;

import java.util.ArrayList;
import java.util.List;

/**
 * ============================
 * Created by Taufik Akbar
 * taufik.akbar89@gmail.com
 * 2020-02-11
 * ============================
 */

public class DataGenerator {
    
    public static List<DayPlan> getAdayInJakarta(Context ctx) {
        List<DayPlan> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.content_image);
        for (int i = 0; i < drw_arr.length(); i++) {
            DayPlan obj = new DayPlan();
            obj.image = drw_arr.getResourceId(i, -1);
            items.add(obj);
        }
        return items;
    }
    
    public static List<DayPlan> getMainSight(Context ctx) {
        List<DayPlan> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.content_image_main);
        String title_arr[] = ctx.getResources().getStringArray(R.array.content_name);
        String time_arr[] = ctx.getResources().getStringArray(R.array.content_time);
        String type_arr[] = ctx.getResources().getStringArray(R.array.content_type);
        String waiting_arr[] = ctx.getResources().getStringArray(R.array.content_waiting);
        for (int i = 0; i < drw_arr.length(); i++) {
            DayPlan obj = new DayPlan();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.name = title_arr[i];
            obj.timeDistance = time_arr[i];
            obj.type = type_arr[i];
            obj.waitingHour = waiting_arr[i];
            items.add(obj);
        }
        return items;
    }
    
    public static List<DayPlan> getCityCenter(Context ctx) {
        List<DayPlan> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.content_image);
        for (int i = 0; i < drw_arr.length(); i++) {
            DayPlan obj = new DayPlan();
            obj.image = drw_arr.getResourceId(i, -1);
            items.add(obj);
        }
        return items;
    }
}
